package pl.tomek.controller;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public enum LanguageProvider {
    INSTANCE;

    private static final String BUNDLE_DEFAULT_CODING = "ISO-8859-1";
    private static final String UTF_CODING = "UTF-8";

    private ResourceBundle bundle = ResourceBundle.getBundle("lang", Locale.getDefault());

    public void setLanguage(Locale locale) {
        this.bundle = ResourceBundle.getBundle("lang", locale);
    }

    public String getMessage(String template, String... params) {

        String format = MessageFormat.format(bundle.getString(template), params);
        try {
            return new String(format.getBytes(BUNDLE_DEFAULT_CODING), UTF_CODING);
        } catch (Exception e) {
            return "Problem in format to UTF-8";
        }
    }

    private void sth() {
        String name = "Tomek";
        String hello = LanguageProvider.INSTANCE.getMessage("hello", name);
    }
}

package pl.tomek.controller;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/log")
public class LogServlet extends HttpServlet {

    private static final String NAME = "login";
    private static final String PASSWORD = "password";
    public static final String ATTRIBUTE_ADMIN = "access";
    public static final String ADMIN = "admin";
    private static final String NO_ACCESS = "no access";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String login = req.getParameter(NAME);
        String password = req.getParameter(PASSWORD);
        HttpSession session = req.getSession();

        if (login.equals(ADMIN) && password.equals(ADMIN)) {
            session.setAttribute(ATTRIBUTE_ADMIN, ADMIN);
            resp.sendRedirect("/SexShop/products.jsp");
        } else {
            session.setAttribute(ATTRIBUTE_ADMIN, NO_ACCESS);
            resp.sendRedirect("/SexShop/login.jsp");

        }

    }
}

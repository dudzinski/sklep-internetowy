package pl.tomek.controller;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "AdminFilter", urlPatterns = {"/productsServlet", "/products.jsp"})
public class FilterServlet implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpSession session = ((HttpServletRequest) servletRequest).getSession();

        if (LogServlet.ADMIN.equals(session.getAttribute(LogServlet.ATTRIBUTE_ADMIN))) {
            filterChain.doFilter(servletRequest, servletResponse);
            //       ((HttpServletResponse) servletResponse).sendRedirect("/SexShop/products.jsp");   - too many redirects  ??
        } else {
            ((HttpServletResponse) servletResponse).sendRedirect("/SexShop/index.jsp");
        }

    }
}

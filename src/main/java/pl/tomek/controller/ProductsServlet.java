package pl.tomek.controller;

import pl.tomek.model.Product;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/productsServlet")
public class ProductsServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.sendRedirect("products.jsp");   //  ??
        String id = req.getParameter("id");
        if (id == null) {
            //wyświetl całą listę
        } else {
            //wyświetl produkt po id
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String desc = req.getParameter("desc");
        String price = req.getParameter("price");
        String amount = req.getParameter("amount");
        String photo = req.getParameter("photo");

        Product product = new Product.ProductBuilder()
                .id(id)
                .name(name)
                .desc(desc)
                .price(price)
                .amount(amount)
                .photo(photo)
                .build();


    }


}

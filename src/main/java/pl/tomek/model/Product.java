package pl.tomek.model;


public class Product {

    private int id;
    private String name;
    private String desc;
    private double price;
    private int amount;
    private String photo;

    private Product(ProductBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.desc = builder.desc;
        this.price = builder.price;
        this.amount = builder.amount;
        this.photo = builder.photo;
    }


  public static class ProductBuilder {
        private int id;        // generowany automatycznie ??
        private String name;
        private String desc;
        private Double price;
        private Integer amount;
        private String photo;

        public ProductBuilder id(String id){
           if(id.matches("[0-9]+")){
            this.id = Integer.parseInt(id);
            return this;}
           else {throw new IllegalArgumentException();}
        }


        public ProductBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ProductBuilder desc(String desc) {
            this.desc = desc;
            return this;
        }

        public ProductBuilder price(String price) {
            if(price.matches("[0-9]+\\,*[0-9]+")){
            this.price =Double.parseDouble(price);
            return this;}
            else {throw new IllegalArgumentException();}
        }

        public ProductBuilder amount(String amount) {
            if(amount.matches("[0-9]")){
            this.amount =Integer.parseInt(amount);
            return this;}
            else {throw new IllegalArgumentException();}
        }

        public ProductBuilder photo(String photo) {
            this.photo = photo;
            return this;
        }

        public Product build(){
            return new Product(this);
        }
    }
}